> This is the personal backlog for @dfosco -- feel free to look around!  
> [← Previous](https://gitlab.com/dfosco/dfosco/-/issues/19) <!-- [• Next →](https://gitlab.com/dfosco/dfosco/-/issues/XXX) -->

##### [Growth Plan](https://gitlab.cultureamp.com/effectiveness/surveys/60d12446afb34f002b69aeb4/processes/6104898aae651186858032fd/feedback_review/new_take_action)

|**W40-W42 Focus**||
|-----|-----|
|🟡 Attend to research methodology | Continue working with Will and Rayana to identify research opportunities, and review existing knowledge, proposed research and methodology in preparation for a study. |
|🟡 Define your milestone workflow | When to start planning, when to wrap up, what to look out for. |

<details>
  <summary>
    Expand
  </summary>

#### Q3 Progress

| Aug | Sep | Oct |
| ------ | ------ | ------ |
| {+========+} | {+========+}  | {+==+} | 
   
| Focus and Actions | |
| ------ | ------ | 
| `Health` | 🟢🟡🔴 | 
| **Results** | Producing high quality work. Providing practical solutions to problems and finding ways to make improvements. Taking the lead and showing ownership of issues. Demonstrating a bias for action. |
| 🟡 Attend to research methodology | Continue working with Will and Rayana to identify research opportunities, and review existing knowledge, proposed research and methodology in preparation for a study. |
| 🟢 JTBDs for Release | Continue working on JTBDs for Release. Define review cadence every ML |
| **Technical Competence** | Demonstrating a high level of domain, functional and/or technical capability. |
| 🟡 Actively track UX Debt / UI polish | Identify and influence the prioritization to fix when it occurs to reduce average days to close (UX KPI) |
| 🟡Increase knowledge of the Release space and continuously share knowledge with peers | Consume existing knowledge about the Release area (uxr insights, competitor evaluation, opportunity canvas) |
| 🟡 Review all user-facing Release MRs | Review all user-facing MRs in a milestone for the Release group, following the MR Reviews Guidelines. |
| 🟡 Contribute to UX MR reviews and incorporate Pajamas into Release work | Starting in FY22-Q4, actively contribute to UX MR reviews and identify opportunities to incorporate Pajamas into your group milestone releases. Embed pajamas tasks on milestone planning. Bring Release design process closer to Pajamas and produce overlapping outputs. |
| **Efficiency** | Prioritizing work and managing time well. |
| 🟢 Prioritize what's most important for Product Designers | Find a Modern Health coach and schedule a call to talk about priority management and decision making |
| **Iteration** | Experimenting with innovative ideas and approaches / being open to new ways of doing things. |
| 🟡 Practice iterative design through the Product Development Flow | Work with the release team to get 1-2 months ahead of development, so that the Build track always has well-validated product opportunities ready to start:
| 🟡 Define your milestone workflow | When to start planning, when to wrap up, what to look out for. |
| 🟡 Achieve consistent milestone progress | Planning, estimation and execution for group design work. |
| 🟡 Small, consistent, breathable iterations| Redefine my own expectations for “core work” output and divide time accordingly to all my responsibilities as Senior Product Designer |

</details>

##### Upcoming OOO

- 🎡
- 🏝 
- 👶
- ☀️ 

##### [XX.X Milestone]()

<!-- 

🔵🔵🔵

| 14.4 | | | Sep 18 → Oct 17 |
| ------ | ------ | ------ | ------ |
| `W38` 18 – 24 | `W39` 27  – 01  |  `W40` 04 - 08 | `W41` 11 - 15 | 

| 14.5 | | | | Oct 18 → Nov 17 | 
| ------ | ------ | ------ | ------ | ------ |
| `W42` 18 – 22 | `W43` 25  – 29  | `W44` 01 - 05 | `W45` 08 - 12 | `W46` 15 - 19 |

| 14.6 | | | Nov 18 → Dec 17 |
| ------ | ------ | ------ | ------ |
| `W47` 22  – 27  | `W48` 29 - 03 | `W49` 06-10 | `W50` 13 - 17 |

| 14.7 | | | Dec 18 → Jan 17 |
| ------ | ------ | ------ | ------ |
| `W51` 20  – 24  | `W52` 27 - 30 | `W01` 03-07 | `W02` 10 - 14 |

 -->

<details>
  <summary>
    Milestone Workflow
  </summary>
  
  ---

- W1
  + Respond to engineering issues that involve UX (already added to to planning if weight more than 1)
  + Work on design issues
  + Work on JTBD & strategy issues 
  + MR Radar
- W2
  + Draft UX issues for next milestones
  + Review UX Debt & UI Polish issues
  + Work on design issues
  + Work on JTBD & strategy issues 
  + MR Radar
- 🔵 W3
  + Work on design issues
  + Finish UX issues for next milestones
  + MR Radar
- W4
  + Work on design issues
  + MR Radar
</details>

### WXX

<!-- 
**Wins of the Week 🌈**

- 1
- 2
- 3

**Learnings 🤔**

- 1
- 2
- 3
 -->

1. [ ] _Category_ - Task
1. [ ] _Category_ - Task
1. [ ] _Category_ - Task
1. [ ] _Category_ - Task
1. [ ] _Category_ - Task

**Later**

<details>
  <summary>
    Expand
  </summary>

---

1. [ ] Task
1. [ ] Task
1. [ ] Task

</details>
