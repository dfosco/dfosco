# Deployment Direction

These are notes based on the [Deployment Direction](https://about.gitlab.com/direction/deployment/) document, general thoughts jotted down to spark discussions and design goals later on. Take everything you read here with a grain of salt 🧂.

_Last updated 22/04/2021_

---

> There is nothing more valuable than user feedback in production. Deployment is the step which brings coded, integrated, and built software to production environments so you can start receiving that feedback. Deployment is part of GitLab's Ops section.

> Deployment **frequency** is increasing because of competitive pressures for organizations to tighten user feedback loops. Adoption of Agile and DevOps practices and culture enable firms to deploy every minute.

Enabling an increase in deployment frequency for high-growth organizations can give them a huge market leverage –— reducing time-to-market for new features and products from weeks to minutes.

> Deployment form is changing because of the adoption of cloud infrastructure, container orchestration and GitOps. Infrastructure as Code and GitOps enables new infrastructure for applications to be deployed in minutes and new application changes to be seamlessly and progressively pulled into existing infrastructure.

More than just enabling new features to be rolled out faster, advanced deployment capabilities reduce operational risk, increase systems reliability and enable entirely new forms of making services available to customers: instant rollouts with feature flags, A/B testing and seamless blue-green deployments.

> One side effect of this evolution is that the line between the configuration of infrastructure and the release of software has been blurred. In order to clearly communicate our vision and strategy for these stages, GitLab's deployment direction is inclusive of both the Configure stage and the Release stage to account for the advancement in deployment practices.

How to create a design vision & practice that respects Configure and Release team boundaries and capacities, while still herding efforts into a unified **Deployment product**?

### Deployment Vision

> GitLab's Deployment vision is to enable deployment to be fast and easy, yet flexible enough to support the scale and operating model for your business.
> 
> We want you to spend the majority of your time creating new software experiences for your users instead of investing time and effort on figuring out how to get it into their hands. No matter if you are operating your own infrastructure or are cloud-native, GitLab is your one stop-deployment-shop from AutoDevOps to building your organization's own platform-as-a-service.

Expanding and flexing our deployment capabilities to meet the needs of our **current** customers that are not actively deploying through GitLab. Moving away from a position that generates feedback like 

- “We don't want to be forced to do things the GitLab way”
- “GitLab is a great SCM solution, but it's not flexible or scalable enough for our needs”

If our Deployments solution is not “sticky” enough to work for current customers that **already see the value** in the GitLab application, how will we become a market leader in this space?

Breaking down the vision:

> We want you to **spend the majority of your time creating new software experiences for your users instead of investing time and effort on figuring out how to get it into their hands**

This section highlights the importance of Configuration as Step 0 for the success of our Deployment vision. A Deployment product that takes 2 minutes to operate, but 2 days to configure will have a hard time succeeding.

> No matter **if you are operating your own infrastructure or are cloud-native**, GitLab is your one stop-deployment-shop from AutoDevOps to building your organization's own platform-as-a-service.

This sentence frames our deployment vision as entirely infrastructure-agnostic, a forcing function that will require our team to vastly increase the surface-area of our product. 

Without going into the usability challenges of an entirely infrastructure-agnostic platform, the investment in backend resources required for this will be enormous.

> No matter if you are operating your own infrastructure or are cloud-native, **GitLab is your one stop-deployment-shop from AutoDevOps to building your organization's own platform-as-a-service**.

Again, a very ambitious proposition, and I love it! It is one thing to enable teams to build deploy pipelines on our platform to ship their products. 

Enabling them to build their own custom infrastructure platforms can increase efficiency on another level, and this is clearly a need for a subset of companies: [Spotify](https://engineering.atspotify.com/2021/03/01/designing-a-better-kubernetes-experience-for-developers/) and [Booking.com](https://www.cncf.io/blog/2020/02/13/after-learning-the-ropes-with-a-kubernetes-distribution-booking-com-built-a-platform-of-its-own/) (GitLab customer) are companies that built fully custom deployment platforms on top of Kubernetes.

### Market

> The total addressable market for DevOps tools targeting the Release and Configure stages was $1.79B in 2020 and is expected to grow to $3.25B by 2024

The market opportunity for deployment tools is enormous, and aligning our Configure and Release features under an easy-to-configure/use/market Deployments product can yield huge returns.

### Current Position

[Link to section](https://about.gitlab.com/direction/deployment/#current-position)

> The CI/CD pipeline is one of GitLab's most widely used features. Many organizations that have adopted the GitLab pipeline also use GitLab for deployment because it is the natural continuation of their DevOps journey.
>
> ...
>
> **Given the amount of choice for deployments within GitLab, there is room to make clear what is the right tool/choice for the job obvious to GitLab users.**

The biggest job for product designers in the Ops section is to make the **happy path** for deployments as clear and easy to use as possible.

> Looking outside of GitLab, we also know there are also plenty of spaces where GitLab can improve. There are users who choose to integrate additional point solutions or deployment platforms because GitLab is lagging in some key capabilities. We also view cloud-native disruption as a competitive threat.

GitLab customers might decide to adopt "last mile" solutions for deployment. It is also increasily easier to roll out your own DIY deployment platform from scratch — Kubernetes is a complex system, but (most) of the current best practices and advanced deployment capabilities can be enabled by it.