# Interview Training

## Interviewing at GitLab

> Notes and highlights from https://about.gitlab.com/handbook/hiring/interviewing/

- When an applicant reaches out asking for a referral, reply:

> Thank you for your interest in GitLab. We would much rather prefer you apply for the position you have in mind directly via our Jobs page. This will ensure the right GitLab team member reviews your profile and replies back to you! Unfortunately at this time, I can not refer you for the position as we have not had a chance to work together. To ensure we stay inclusive, I can also not influence your application".

- When scheduling interviews, please mark these are private on the calendar. Many team members at GitLab have their calendar event details set to be internally viewable. Setting up interviews as private is important to maintain confidentiality of applicants/candidates during the interview process.

### Typical Hiring Timeline

1. Prior to interviewing, the talent acquisition team will identify the most qualified candidates for the vacancy by reviewing Internal Applicants, searching the GitLab Talent Community, reviewing Employee Referrals, and searching through a variety of sources including, but not limited to LinkedIn.

2. The talent acquisition team and/or hiring team does the first round of evaluations by reviewing candidate profiles. This team will refer to the country hiring guidelines before moving candidates forward.

3. Pre-screening Questionnaire: Some candidates will be sent a pre-screening questionnaire relating to the position to complete and return to the sender. The questionnaire and answers are kept within the candidate's Greenhouse profile.

 - Team members who review the pre-screening questionnaire answers should refer to the private GitLab project that holds guides on how to review each of the questionnaires. Contact your assigned Recruiter for a direct link to the guides.
 - When a candidate returns their assessment, the talent acquisition team member who sent the assessment and the hiring team member who was chosen to review it will receive a notification. Once a reviewer submits the feedback for the assessment in Greenhouse, the talent acquisition team will be notified.
 - Candidates that have satisfactory assessment results may be invited to a screening call. Disqualified candidates will be sent a note informing them of the rejection.

4. Screening call

 - If the candidate qualifies for continued consideration, one of our recruiters will conduct a screening call using Zoom and scheduling it via Greenhouse.
 - A member of the employment team will move the candidate to the "Screening" stage in Greenhouse. They will reach out to the candidate to collect their availability and then send out calendar invitations to both the interviewer and candidate.

5. Technical interview (optional): Certain positions also require technical interviews.

6. Behavioral interview: Some roles include a behavioral interview with a team peer or leader. Behavioral interviews may be conducted as panel interviews.

7. Further interviews:  All interviewers will assess the candidate's values alignment by asking behavioral questions and scoring the values alignment as part of their feedback form in Greenhouse. Additional interviews would typically follow the reporting lines up to the CEO. For example the technical interview may be conducted by an individual contributor, with subsequent interviews being conducted by the manager, director, executive team member, and then potentially the CEO.

 - Interviewers will follow the same "no show" policy as the recruiters. If a candidate does not show up or reach out to the team, they will be disqualified.

8. References: The hiring manager or the hiring team will contact references for promising candidates. References will be collected towards the end of the interview stage for final candidates, and they must be checked before an offer is made.

 - Three references will be requested, but at least two references need to be completed, and at least one needs to be a past manager.
 - The talent acquisition team will move the candidate to the "Reference Check" stage in Greenhouse, and email the candidate to request their references' contact details.
 - After the reference checks are completed, the person performing the reference check will input a scorecard in Greenhouse with their findings.

9. At the same time as starting the reference check process, the talent acquisition team will start the background check process.

10. Offer package: After reference calls are completed successfully, the talent acquisition team moves the candidate to the "Offer" stage and submits the offer package in Greenhouse for approval.

11. CEO interviews: The CEO may choose to interview candidates in a last round interview after reviewing the offer package.

12. The recruiter, hiring manager, executive, or CEO should make an offer verbally during a call with the candidate, and it will be followed with an official contract as described in preparing offers and contracts.

13. The talent acquisition team will, if applicable, add language to the contract that states that employment or engagement is contingent on a valid work permit or visa. A start date should factor in that the approval of a new work permit may take several weeks.

14. The manager follows up to ensure that the offer is accepted and that the contract is signed.

15. Candidate Experience Specialists starts the onboarding issue.

16. The hiring manager considers closing the vacancy.


### Inclusive Interviewing

- Best Practices: we will make our best effort so that each candidate's full interview panel has at least one non-male GitLab team-member on it, with many teams already committed to this for every single interview panel.


### Moving Candidates Through The Process

- In an effort to streamline the hiring process, improve the candidate experience, and hire talent faster, best practice is to coordinate interview times so that candidates can complete the process within 2 weeks. (The initial screening call and optional CEO interview are not considered to be part of the 2-week goal.)

- If the process before or during the team interview is taking more than a few days to confirm, the Recruiter should reach out to the candidate, apologize, and explain what is going on.

- Best practices:
  - **Those on the interview team should prioritize the interview in their schedules.** Hiring an amazing team is critical for GitLab, and how we spend our time shows where our priorities are.
  - **Maintain candidate confidentiality**: All candidate names and details are kept confidential within the hiring team to avoid bias or the potential to jeopardize a candidate's current employment as well as to maintain data protection. The only people who should have access to details about candidates are Talent Acquisition, People Ops, the hiring manager(s), approved interviewers or reviewers within that team, the executive of the department, the legal team, the CFO, and the CEO.
    + Do not include identifying personal details in your feedback notes for a candidate.
    + Anytime you want to discuss a current, past, or potential candidate, please do so privately (whether in a private Slack channel/message, email, or within Greenhouse). If you have access to it, you can also provide the direct Greenhouse link and avoid mentioning names or identifying details.
    + Emails from the candidate are synced on our ATS, and for that reason, the entire hiring team for that position has access to it. Remember to ensure any sensitive information is marked as secret/private in the candidate profile.
  - **Remember to inform candidates about what stage they are in.** For example, if in the hiring process for the particular position / team you've agreed that there will be four stages, be sure to inform the candidate of where they are in the process during each call / stage. To better manage candidates’ expectations, at the end of the interview, let them know what stage they are in as well as what the next step/stage will be **if** they do pass this interview. Considering we are speaking with other candidates, they can expect to hear back within a couple of days.
 - **The process can differ from team to team and from position to position.** If a candidate submits a resume to a particular open position and is being considered for another open position, send a short note to update the candidate and get their approval as well as to inform them that their process may be slightly different than previously expected or delayed. If the roles are on different teams, the candidate will ideally only move forward with one, depending on their interests and qualifications. If the candidate is being rejected for one or all of the positions they applied for, they will be notified of which vacancies they are being rejected for.
- **Compensation is discussed at start and end but not in between.** If the manager has a question about compensation, please ping the People Ops Analyst for review. If the question needs to be escalated, the People Ops Analyst will add the Chief People Officer to the conversation.
- **An approval team authorizes all offers.** The manager proposes a suggestion for an offer (including bonus structure if applicable, etc.) as a private comment in Greenhouse and informs the talent acquisition team on its details depending on what is applicable. 

### Rejecting Candidates

**Rejecting and Feedback**

1. **At any time during the hiring process** the candidate can be rejected.
2. **Everyone who interviews a candidate must complete a scorecard** and are required to input pros and cons as well as an overall recommendation.
3. **If a situation arises in which the role has been filled within 24 hours of a candidate's interview, we will not cancel the interview.** 
 - The interviewer will take the call and be transparent with the candidate about the situation upfront. It would be a good use of time to discuss other roles, future roles or questions about GitLab in general.
If the candidate's interview is outside 24 hours, the interview will be deleted in Greenhouse before the candidate is rejected and notified.
4. **The candidate should always be notified if they've been rejected.** The talent acquisition team is primarily responsible for declining the candidate, but the hiring manager should be prepared to let the candidate know why they were declined if they had progressed to the team or manager interviews. The hiring manager can also share this feedback with the talent acquisition team, who will relay it to the candidate. [See table with specifications for rejection notice](https://about.gitlab.com/handbook/hiring/interviewing/#rejecting-candidates)
5. If people argue with the feedback that we provided:
 - Do not argue with or acknowledge the validity of the contents of the feedback.
 - Share their feedback with the people involved in the interviews and the decision.
6. The talent acquisition team may send out an inquiry to candidates to gather our own feedback after they have exited the hiring process.

---

## Unsconscious Bias

> Notes from: https://about.gitlab.com/company/culture/inclusion/unconscious-bias/

**Unconscious biases are stereotypes about certain groups of people that individuals form outside their own conscious awareness.** Nearly all our thoughts and actions are influenced, at least in part, by unconscious impulses. There’s no reason bias should be out of scope.

Biases help the brain create shortcuts for the decision-making process and detect threats. Our unconscious biases are based on our own experiences and they help us detect patterns and find in-groups, a basic survival mechanism below our conscious radar. **If unconscious bias goes unchecked, it can lead to fixed general views of how people should act or behave, and/or negative out-spoken attitude towards a person or group.**

Everyone has unconscious biases, the goal is to bring them to our consciousness and navigate them. In order to provide a more inclusive and empathetic work environment.

### How do I recognize it?

- **Gain self-awareness and be present**: Pay attention to your body language and tone. Instead of reacting, set up a response. Monitor your self-talk.
- **Exhibit tolerance and patience; avoid assumptions**: Choose to listen, pause, and consider first. Offer the benefit of the doubt. Don't jump to conclusions. Ask questions.
- **Make thoughtful impressions and decisions**: Challenge your own default views. Take all options into consideration. Use informed decisions, not just your "gut".
- **Seek feedback and choose to use it**: Don't avoid the conversation, seek it out. Ask for the feedback you need, not want. Achieve by understanding, not defending.
- **Remain open-minded and continue learning**: Hold yourself and others accountable. Revisit, reflect, and review your day. Diversify your circles.

### Unconscious Biases to look out for (in ourselves and in others)

- **Affinity Bias**: Biased towards people "who make me comfortable"; Biased against people "who make me uncomfortable".
- **Affective Heuristic Bias**: Immediate emotional judgement influenced by superficial traits such as race, gender, age, ornames.
- **Ageism Bias**: The tendency to have adverse feelings/perception about another person based on age.
- **Authority Bias**: The tendency to attribute greater knowledge to persons in positions of authority than they may actually possess.
- **Confirmation Bias**: People look for ideas or findings that confirm their existing belief.
- **Conformity Bias**: Tendency to take cues for proper behavior based on the actions of others. Example: studies show thatpeople are more likely to donate to charity if they know/see others donating.
- **Contrast Effect**: A cognitive bias that distorts our perception of something when we compare it to something else, byenhancing the differences between them. For example, your performance evaluation of a team member is affected by the evaluation that you wrote for a different team member just before.
- **Gender**: The tendency to prefer one gender over another gender.
- **Height**: Tendency to judge a person who is significantly shorter or taller than what is “deemed as socially accepted”human height.
- **Halo Effect**: Form of bias which favors one aspect that makes a person seem more attractive or desirable. Example: If wethink someone is good looking we might also think that they are intelligible and charismatic.
- **Horn Effect**: Opposite of Halo Effect. Form of bias that causes one's perception of another to be overly influenced by one or more negative traits. Example: Someone who has failed the project is always like that and incapable of improving.
- **Name Bias**: Form of bias which favors a person based on their name and the perceived origin of their name.

### Tips on Recognizing and Avoiding Bias

 1. Understand we all have biases
 2. Determine what your biases are
 3. When you see it, block it
 4. Be ok with having an opinion different from the group
 5. Be wary of first impressions
 6. Do research on stereotypes
 7. When working globally understand that your perceptions of bias may simply be the result of a lack of understanding of cultural differences
 8. When making critical business decisions, invite others who can broaden your perspective to help ensure there are no hidden biases in the decision

### Strategies for Managing Unconscious Bias in Practice

The [SPACE2 Model of Inclusion](https://cultureplusconsulting.com/2018/10/17/six-proven-strategies-for-managing-unconscious-bias) - Six evidence based techniques for managing bias in oneself and others:

- Slowing Down
- Perspective Taking
- Asking Yourself
- Cultural Intelligence
- Exemplars
- Expand

### Practical ways to reduce or avoid the impact of bias

**As an application reviewer or interviewer, if you find yourself biased positively or negatively, excuse yourself and ask someone else to review or interview that applicant. Being positively or negatively biased towards one candidate is unfair to all candidates.**

---

## Inclusive Language

> Notes from https://docs.google.com/presentation/d/186RK9QqOYxF8BmVS15AOKvwFpt4WglKKDR7cUCeDGkE/edit#slide=id.g594d890234_0_0

### GitLab’s definition of Diversity & Inclusion

**Diversity is the ability to be able to recognize, respect and value differences based on several diversity dimensions**, such as race, gender, age, ethnicity, religion, national origin, disability, sexual orientation, etc. 

It also includes an infinite list of individual unique characteristics and experiences, such as communication style, career path, life experiences, educational background, marital status, military experience, parental status and other variables that influence personal perspectives. These life experiences form how we as individuals approach challenges and solve problems such as how we may react and think differently, make suggestions and decisions, etc. 

Diversity is also about diversity of thought. Here at GitLab **we recognize that having this is important to business performance and requires tapping into these unique perspectives so that we do have a diverse workforce where everyone feels that can show up as their full self at work each day.**

**Inclusion is the ability to understand diversity and to incorporate inclusive actions regardless of diversity dimensions.** 

**It is a connection of diverse perspectives to come together where innovation can take place.** It is the power of GitLab Team Members all showing up for the common goals of the company and each other.

### Additional Cultural Definitions 

**Ableist language** is any word or phrase that devalues people who have physical or mental disabilities.  It’s appearance often stems not from any intentional desire to offend, but from our innate sense of what it means to be normal.

**Cultural Destructiveness** Negating, disparaging, or purging cultures that are different from your own 

**Cultural Incapacity** Elevating the superiority of your own cultural values and beliefs and suppressing cultures that are different from your own 

**Cultural Blindness** Acting as if differences among cultures do not exist and refusing to recognize differences 

**Cultural Pre-Competence** Recognizing that lack of knowledge, experience, and understanding of other cultures limits your ability to effectively interact with them 

**Cultural Proficiency** Honoring the differences among cultures, seeing diversity as a benefit, and interacting knowledgeably and respectfully with a variety of cultural groups

### What is Inclusive Language and Examples

**Language that avoids the use of certain expressions or words that might be considered to exclude particular groups of people, especially gender-specific words**, such as "man", "mankind", and masculine pronouns, the use of which might be considered to exclude women as an example

**Limiting Language**
 - Boyfriend, girlfriend (when sex/gender is unknown)
 - Mailman
 - Sexual preference
 - Homosexual (male)
 - Homosexual (female)
 - Transgendered
 - Freshmen
 - Manning the table

**More Inclusive Language**
 - Person you are seeing, involved with; partner; spouse
 - Mail carrier
 - Sexual orientation
 - Gay
 - Lesbian
 - Transgender
 - First year student
 - Staffing the table

### Applying Inclusive Language Practices

**Consider Your Assumptions**
Whether you’re drafting an e-mail message or preparing a keynote speech, it’s important to carefully consider the people you’re communicating with. **Ask yourself about the unconscious assumptions you’re making about your audience. Rework anything that ignores diversity or might alienate individuals or groups.**

**Be Safe With Pronouns**
Phase out gender-specific “he” and “she,” unless you unequivocally know that your audience identifies as one or the other. 

**Remove Ableist Language**
It has become common to use descriptive words like “crazy,” “dumb,” etc. in the workplace. Words that denigrate in one usage can attach to those connotations into their other usages. In addition to reinforcing stereotypes about mental health conditions and physical disability, careless words can shame the person on the receiving end of your message—no matter your intention.

**Understand There Will Be Resistance**
Some people will resist a call for more inclusive language in the workplace.  Old habits can be hard to break, especially if people are set in traditional ways.

## Greenhouse 

> Notes from: https://about.gitlab.com/handbook/hiring/greenhouse/#scorecards

### Scorecards

The scorecard consists of a few elements. In general, they feature text boxes for adding notes and text boxes for addressing specific questions. To note, if a text box is required, there will be a red asterisk by the question.

Underneath the first text box, Key Take-Aways, there are two additional links, Private Note and Note for Other Interviewers, which will open additional text boxes. **A Private Note is typically used by the Talent Acquisition Team when collecting compensation information and it's only viewable by the Talent Acquisition Team and Hiring Managers for the requisition.** 

The **Note for Other Interviewers text box is extremely useful to all Interviewers**, as it's where you can include information that you think would be relevant to future Interviewers. **For example, specifying areas to dig into, further evaluate, or look out for. Any notes in this field will appear in the next Interviewer's Interview Kit on the Interview Prep tab.**

**We want to highlight the strengths and weaknesses of the candidate in an easy to absorb, standardised way. Every scorecard must include Pros and Cons**. This helps the talent acquisition team gather data that will be presented to the candidate in the form of feedback.

Below the text boxes for notes, each role has a list of desired attributes. **Each stage has certain attributes highlighted that are recommended points of evaluation.** However, no attributes are required, so you're welcome to rate any attributes you've gained insight into. **All stages include attributes for values-alignment**, which all Interviewers are heavily encouraged to complete, so we can assess values-alignment for each candidate.

**Below the attributes is the final piece where you will make your decision.** Greenhouse says "Overall Recommendation: Did the candidate pass the interview?". **This should be interpreted as, "Do you want to hire this candidate?"** answered appropriately. The final score is not required by Greenhouse, but it must be completed by the Interviewer. If the Interviewer does not add their vote, the Talent Acquisition Team will follow-up to fetch their vote. **If you are on the fence about a candidate, that's typically an indication that you do not want to hire the candidate.** Though, if you're really unsure, feel free to reach out to the Recruiter to discuss further. Your Recruiter may agree with your hesitations and decline or you may agree that there is an element that should be explored in an additional interview.

**In order to ensure a timely response for each candidate** and to reduce the possibility of forgetting important details and impressions of the candidate, **all scorecards should be submitted within 3 business days of the interview.**

**In order to help remove bias, Interviewers (unlike Hiring Managers) are not able to see another Interviewer's scorecard until they've first submitted their own.** If there are certain flags or notes that should be highlighted to the rest of the Interview Team, the **Interviewer should add a note in their scorecard by clicking the Note for Other Interviewers section** right underneath the Key Take-Aways text box in their scorecard. 

### Interview notifications

**You can set up reminders in Greenhouse by going to your account settings and turning on Daily interview reminder email which will email you each morning with a list of your interviews for the day.** You can also connect your Slack account to your Greenhouse account and receive reminders after the interview is over if your scorecard is still due, as well as other notifications depending on your settings.

### Leaving Notes

It's always recommended to leave notes in a candidate profile to maintain communication between the Hiring Managers, Interviewers, and Talent Acquisition Team. To leave a note, go to a candidate's profile, and, on the right side under `Make a Note`, type your note and tag anyone you like to see the note. **If you do not tag anyone, no one will receive a notification**. Once you're done with your note, click Save and it will notify anyone tagged and be logged in the candidate's profile in the Activity Feed. Anyone who has access to the requisition will be able to view it there.