# Making User Experience happen as a team

https://www.linkedin.com/learning/making-user-experience-happen-as-a-team

---

## 1. Why Politics Matter

### Getting Things Done

- UX work that does not involve the development team from the start will be framed as "extra work" or even "rework"

- Getting things done gets easier when you involve more people

---

### Managing Workload

- Doing design as small ad-hoc tasks based on team requests (usability testing just before a project goes live, "designing a screen", etc) serves to frame design as a "service" that is optional

- Not as a discipline that can help guide a product towards its best possible outcome

- And definitely not as the people who own product definition: defining a roadmap based on user needs, doing research to validate the direction, doing early design explorations, prototyping -- a cohesive feature-set driven by user experience

- Projects:
  + Strategic
  + Could have been Strategic

---

### Integrating with Teams

- Way past the time designers should be isolated from developers

- "Governance Role" is also a bad idea: you become the bottleneck that just checks a box

- Better ways:
 + Coding a framework that supports developers
 + Early involvement that ensures they get it right

- These will:
 + Add value
 + Reduce coding time
 + Reduce errors and rework
 + Help the product team look good
 + Provide real metrics

- Teams will start to be excited to work with a UX designer, and the whole team will be happy to have a more successful product, that better resonates with their users.

---

## 2. Integrating with Development Teams

---

### Empathizers and Systemizers

Empathizers

- Want to identify others' emotions and thoughts
- Intuitively figure out how people are feeling
- Relaxed about details
- Develop language rapidly
- Sociable and communicative

Systemizers

- Want to analyze, explore, construct systems
- Intuitively figure out underlying rules of how things work
- Less interested in chatting
- Less sociable

- All of us have a preference for empathizing or systemizing
- Lots of people in tech tend to the systemizing side

**Seeing is believing**

- Have people with lower natural empathy watch users interacting with the products, to build a better shared understanding of the user needs
- Ask these participants to take notes on what they see and hear, and later help them debrief the session they just watched
- Analyze that data and get together with the team to help them find the underlying themes
- Systemizers tend to like storyboards and scenarios, because they make things concrete and _show the relationship between user needs and the underlying system_
- Pairing across disciplines can help systemize and empathize around the user needs, and the product ends up better because of this collaboration


---

### Foot in the Door

- Identify the teams you want to work with: the ones that generate the most revenue, or the ones that will give you quick wins that you can broadcast across the organization
- Find partners: Receptual individuals that are excited to work with you and want to get involved in user experience work. Individuals with indirect authority.
- Choose the timing: Be ahead of the work the team is doing, during product definition phase and provide useful data to influence the _next_ release — rather than playing catch up and causing re-work
- Sell user experience data: make sure the data you collected is actionable and can be used by the team in their development process
- The same goes for user experience techniques: workshops and other activities help sharing knowledge and solidifying the impact of UX in the product

### Earning Respect

- Display ownership and commitment to the development team: be truly present, own issues and see them through resolution – be action-oriented
- Show you're willing to take on the work to back up your changes. This will help framing design as an acceptance criteria
- Sit with the team: see how they work, who they turn to for advice, how they resolve issues, what is their definition of success
- This will help you understand whose trust you need to win in order to be successful on the team
- Also give the team access to the users, so they start seeing their customers real needs

- Understand how much impact you will have on a team vs. the time you will invest in working with them
  + Some teams require a small design involvement -- perhaps a usability analysis, or a design iteration
  + Others will require a larger design process, and investing in the relationship with them becomes more important

### Own the user experience

- Developers are the ultimately responsible for how a product is shipped, how it looks, and how it behaves
- The team needs to see you as the person who they go to for UX guidance: it might mean design deliverables that help shape the product, or guidance on design system guidelines and usability recommendations
- Don't be the design bottleneck: invite others design contributions, and if there are "bad" suggestions, maybe let them become a part of the solution as an intermediary step to guide it to a better place.

- Predictive UX work, vs. Reactive: That way you have the answers before the team asks questions
- Complete user and system modelling
- Show benefits of user experience work before it's actually "required" to unblock development
- Work within the team process: issues, bug trackers, trello, JIRA... you need to be where the work is.

## 3. Communicating UX

### Dollars, Dollars, Dollars

- Biggest mistake around research: presenting the _research findings_, rather than presenting _what those findings mean_
- "Start your sentence with a dollar figure to help more impact"
- Efficience, Effectiveness, Satisfaction: each of these measures has a dollar value attached to it
- Help frame user needs in business terms
- Tracking the impact of your design changes after release

### UX as a Brand

- What is your brand? What do people think when they hear the word "UX" from you?
- “I make computers easier to use”
- Find an expressive way to communicate the role and work your UX team does.
- Then, market the team to the org: identify your audience, your message, and the medium you plan on using.
- What is your pitch to: C-level team, Managers, ICs? What works for _your organization_?

### The danger: Too much work

- Having the members of a UX team with limited size is not sustainable
- How do you prioritize UX work that needs to be done, while delivering the required impact to the organization and not overworking designers?

- Are there any high-cost high-visibility projects that need to happen? Or a maintenance project that is used by a large volume of users?

- These might be good candidates for UX involvement. Realistically speaking though, the development team's willingness to work with UX should also be a factor. Also: are the teams early enough in the project that change is still viable? Does the team has a budget for your involvement?

- Last of all: what value does the project brings to _you_? A high visibility project with strong sponsorship might have more value to your team in terms of promotions, visibility, learning, and recognition. Take that into account.

- Split your projects and order them by priority. Then, calculate the effort required for each project or sub-project (UX weight) and fill up your team capacity.

- Draw a line on the maximum capacity of your team, with the non-priorities falling beneath it. Any project that wants to get within the priority line will either:
  + Push another project from the top under the line
  + Bring in more full-time resources to the team

- Take into account how many simultaneous projects your team members can handle: anything more than 4 likely means someone is getting overworked.

- This priority model is helpful to keep track of changing organization goals, and how they affect your team: any shift in goals can be reflected in your priorities, in a way that is respectful to your team members, while staying accountable to the organization.

- Having a priority model changes conversations from "you need to accomodate my needs" to "how do your needs fit the organization goals"

### Resources for teams you can't help

- What can you do instead of actual hands-on design work for low priority projects?
  + Lightweight design consulting
  + If they have budget: suggest external vendors, while helping structure the project or relationship with the vendor
  + Provide UX guidelines so they can run their own design processes
  + Styleguides and pattern libraries can be helpful to empower good design decisions for UI & Front-end

- A good library of design documentation can help other teams better understand design processes and terminology. They might not product good design on their own, but they will certainly be in a better position to collaborate with UX teams in the future.

## 4. Having a Voice and Something to Say

### First Say No

- It's only when you stop doing extra work that people will realize your value

- Until internal clients start complaining you're not available, your management won't know how overworked you truly are

- Saying no in an assertive way: 
> “No, I won't be able to help with that”; or
> “No, I won't be able to help with that, our team is already committed to other projects”

- Don't add too much information, though: the requester may take in upon themselves to reach out to other stakeholders in your plate and try to free you up 

- You may also take some time to answer, and tell the requester you will get back to them. That will give you more time to formulate an appropriate answer.

### Speak from data, not opinion

- You need to have readily accessible, easily digestible data
- Push it out into the world, rather than waiting until it's requested
- Make a repository of user data that can be used in meetings, presentations, etc
- White papers, industry research are other useful data sources

- Bring developers and other stakeholders into research sessions, and then conduct workshops with them to analyze this data and make sure the insights are applied to the product as it's designed and built.

- Each time you introduce real user data, you help overcome team member inertia or indiference to user needs: more exposure means more understanding

- Team metrics: 
  + Usability issues ××× open, ××× fixed
  + UX feature revisions: ×××
  + Documented design decisions: ×××
  + Maybe even “Days of development prevented: ×××”

- Essentially, you're tracking the output and value of your combined efforts. Being able to quantify the impact of your UX work has on the development process is pretty good insurance for your future employment and career growth.

### Influencing without authority

- When we fail to influence a team, it might be a problem with our approach
- Figure out what the product team wants, and then show how you can provide it

- There's a big need for communication, since many times the team does not understand the skills and work a UX designer is able to carry: you need to listen more than you talk

- For example, when advocating for user research with developers, a designer could argue that

> “With user research, we can find show-stopped issues before you spend the time to code them and have to do re-work”

- Perhaps they are not engaged in the re-design effort you are working on — and that's on _you_ to make it clear.

- Relationships matter: UX people usually have a lot of contacts because they work with many people across the organization. Draw on those people to help you out.

> Note: Usually for me, that has been in the form of mentorship and advice, either on how to deal with tricky team dynamics or how to strategize for complex projects

- Because of our position, UX people are usually strategically aware of what's going on in the organization: making that visible to your immediate team can bring a lot of value

- Your authority comes from your knowledge and skills, not (necessarily) from your position in the org chart.

- Understand the team's goals and perspective, and then embed your UX goals within a shared proposal that focuses on both

## 5. Growing Up

### UX Maturity Model

- Level 1: Hostility towards usability
- Level 2: Developer-centered usability
- Level 3: Skunkworks usability
- Level 4: Dedicated usability budget
- Level 5: Managed usability
- Level 6: Systematic usability process
- Level 7: Integrated user-centered design
- Level 8: User-driven Corporation

### Cultivate Executive Champions

- Ask your executives what you can do for them:
  + What do they need?
  + What do I have to further their agenda?
  + How can I make them look good?

- Understand how you can slice your data to make it useful for them
- If they see you as a source of impartial, user-centric data, you become more important for the company's strategy 
- To talk with business people, you need to talk in business terms: talk in terms of "customers" and "employees", rather than generically referring to "users"
- Don't talk process, talk outcomes (and how they impact the company bottom lines)
- Time to market, conversion, cost savings, dollars per year
- Before and after snapshots of system and metrics

### Justify your budget

Getting budget to expand your team:

- Communicate team support
- Capture your success metrics
- Obtain testimonials from other teams

- **Never, ever complain that you're lacking budget**
- Just show how you could use extra funds, and how this allocation would benefit the business
- How much do you need, and what will you be doing with it?
- Always good to have contingencies, in case your first choice cannot be met: Better tools? External vendors? Partial allocation from other parts of the org?

- Have the data that shows the best decision is to give you what you want

## 6. Conclusion

### Next steps

A handy summary and to-do list:

- Work out what it takes to sell UX to the correct people in the organization
- Figure out your team's work priorities and stick to them
- Create a priority model and update it frequently

**Plan Ahead**

- Executive leadership
- Visibility

**Goal: Become the group responsible for defining user experience**

- Enlist the help of every product team member
- Always speak from data, not opinion