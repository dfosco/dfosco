# Cultivate Equality at Work 

## Understand the Impact of Unconscious Bias on Employee Performance

> Notes from https://trailhead.salesforce.com/content/learn/trails/champion_workplace_equality

### Bias: What It Is and Where It Comes From

Most of us probably believe we are not prejudiced. We probably believe ourselves to be ethical and unbiased, too. In the workplace, we probably believe we’re good decision makers, capable of objectively deciding about a job candidate or employee’s performance, and reaching a rational and fair conclusion about any particular business problem or situation. Yet it’s clear from more than 2 decades of research that we all have bias.

### Bias and Unconscious Bias: The Impact on Race and Gender

- When shown photographs of men with similar body types, evaluators rated the athletic ability of black men as higher than that of white men.
- When evaluating the quality of verbal skills as indicated by vocabulary definitions, evaluators rated the subjects lower when they were told they were black, compared to when they were told they were white.
- Randomly assigning different names to resumes showed that job applicants with “white-sounding names” were more likely to be interviewed for open positions than were equally qualified applicants with “black-sounding names.”

As human beings, we have the ability to critically think and analyze. In the workplace, **it is important for us to understand when we are relying on our impulses driven by unconscious bias, and to challenge them**—so that in the end we make more informed and rational decisions, and do not unintentionally exclude anyone.

## Recognize Bias in the Workplace

> Notes from https://trailhead.salesforce.com/content/learn/modules/workplace_equality_inclusion_challenges/we_inclusion_challenges_bias_in_the_workplace

### Bias at Work

Unconscious bias acts as a barrier to Equality. It prevents us from cultivating diverse talent, developing an engaged workforce, leveraging unique experiences and perspectives, and sparking innovation through collaboration. **Bias at work can appear just about anywhere, but most often in recruiting, screening, performance reviews and feedback, coaching and development, and promotions.**

While many of us would never say or truly believe any of the [biased statements listed in the article above], our unconscious minds may be causing us to make decisions based on these archetypes created by the media and cultural misunderstandings. Let’s take a closer look at where bias shows up in the workplace, and how.

### In-Group/Out-Group

As humans we tend to be drawn to what is familiar to us. **In-group bias is the process of favoring someone who is similar to you and excluding those who aren’t in your natural or immediate group.** This can have significant impact in the workplace. However, we can challenge this by reaching out and connecting to those who don’t seem immediately like us. And often, you’ll find that there are in fact many similarities. 

#### Interviewing/Hiring

**If you look for talent in the same places, the same schools, using the same referrals, you’ll get the same kinds of candidates.** Additionally, if the hiring team doesn’t have a diverse representation of candidates, it will be difficult to look for and see diversity in your slate. Objective hiring criteria is also important to help eliminate bias and promote Equality in the hiring process.

#### Coaching/Development

**Assumptions and stereotypes can impact who gets those more interesting and perhaps complex assignments** (heavy travel or assignments abroad). **Feedback will be delivered in different ways, either more directly or indirectly, particularly between different genders.**

#### Talent Reviews

Ask yourself the following questions regarding measuring and evaluating performance. **Are the tools used to review employees free of bias? What criteria are being used to calibrate performance evaluation? Is it skewed to different types of personalities? Are you being intentional with promoting a diverse set of employees?**

#### Day-to-Day Interactions

Communication between peers, managers, and employees takes all different forms. **Some individuals can get to the task right away, others need more time to relate in order to trust others to act on and meet deliverables. Recognition and reward take different forms, and it’s important to understand what motivates individuals to come to work, perform, and stay engaged.** The key is to treat people equally. To do so means you have to be aware of your biases. Remember, our brain is leading us—unconsciously!

### The Positive Impact of Diversity and Inclusion

By introducing greater diversity in the people we hire, develop, and promote, and by proactively becoming more inclusive in our behavior, we can improve the unintended negative effects of unconscious bias and reap the benefits. As the research has shown, diversity in the workplace, in all its forms:

Leads to better results—financial, customer satisfaction and even philanthropy
Fuels diversity of ideas and innovation
Is representative of global customer profiles and buying decisions

## Understand the Meaning and Power of Allyship

> Notes from https://trailhead.salesforce.com/content/learn/modules/workplace_equality_ally_strategies/we_equality_ally_get_started

### What Does It Mean to Be an Equality Ally?

According to Merriam-Webster’s dictionary, an ally is someone “joined with another for common purpose.” **An ally is an individual who doesn’t identify as someone from an underrepresented, marginalized, or disenfranchised group—but seeks to understand the issues that impact these communities and use their platforms to create change.**

One ally can be extremely powerful and make a difference in someone’s life—but **imagine the power of a community of allies. If a large group of people can come together to amplify the voices of others and create a better environment for all, that’s when we can start to see tangible progress in Equality.**

### Key Terms

**Underrepresented**: This refers to a group whose representation in an organization is disproportionately lower than their proportion in the general population in the country and communities around them. In the tech industry, this often refers to women, black, and Latinx employees but can also refer to Hawaiian native, American Indian, and those who identify as two or more races. 

**Disenfranchised**: A group that is often marginalized and lacks power within their political, legal, and economic systems. Groups such as the LGBTQ community, people with disabilities, underrepresented minorities, people of lower socioeconomic statuses, and so on, are often considered disenfranchised. 

**Diversity and Inclusion**: As defined in the Inclusive Leadership Practices module, Diversity is the presence of people from many different backgrounds and identities. Inclusion is when every single person in the community is valued, heard, respected, empowered, and feels a true sense of belonging. Only when you combine the two do you foster a true culture of Equality. 

**Employee Resource Group**: Employee-led and employee-organized groups centered around people with common identities/backgrounds and their allies. At Salesforce, our groups are Abilityforce, Asiapacforce, BOLDforce, Earthforce, Faithforce, Latinoforce, Outforce, Salesforce Women’s Network, Southasiaforce, and Vetforce. 

**Microaggressions**: According to Psychology Today, “Microaggressions are the everyday verbal, nonverbal, and environmental slights, snubs, or insults, whether intentional or unintentional, which communicate hostile, derogatory, or negative messages to target persons based solely upon their marginalized group membership.”  These are often subtle but painful experiences for underrepresented groups—like getting thousands of paper cuts throughout the work week. As allies, we can create a more inclusive workplace by decreasing and interrupting microaggressions.

**Advocate**: Someone who supports or promotes the interests of a cause or group, for example, an advocate for disability rights.

**Activist**: An activist goes beyond support of a cause or group by taking action to create political and social change in the form of leadership, protest, legislation, or public influence, for example, activists across the world organized local women’s marches. 

### The Power of Allyship

**Underrepresented groups often cite the feeling of being “an only” in the room. It’s often described as feeling isolating and intimidating—especially if something happens that only impacts that specific group.** This can include things like a male colleague saying something sexist to a woman in a meeting or a current event that harms a specific ethnic group. **We have the power to ensure that even if someone may be an only in the room, they never feel powerless because they know that they are surrounded by passionate and caring allies.**  

## Beginning Your Ally Journey

> Notes from https://trailhead.salesforce.com/content/learn/modules/workplace_equality_ally_strategies/we_equality_ally_be_good

### Approach Allyship as a Continuous Path

Remember that **becoming an ally isn’t instantaneous**. It also isn’t simple. There can be many missteps along the way, and **it takes time to understand the issues that impact a community and to find your voice or next action to create change.** That’s okay. This is a journey, and as long as we move forward with empathy, good intentions, accountability and forgiveness, we can find our way. 

### Remember Intention vs. Impact

Amy Lazarus, CEO and Founder of Inclusive Ventures, always starts off Equality discussions by reminding everyone of intention vs. impact. In other words, **the majority of the time people have positive intent; however, it’s the impact of our actions and words that matters.**

Think about times when someone expressed they felt hurt or disrespected by something that was said. The immediate reaction by the person who said it is usually defensive. This is where we hear “I’m not racist/homophobic/sexist and so on”. **Imagine if instead we responded with, “I’m sorry, please teach me what I said/did wrong.” This helps us have brave and honest conversations that we can learn from.**

### Prioritize Accountability and Forgiveness

You will see this mentioned in other modules as well: **Accountability and forgiveness go hand in hand when we are having difficult and powerful conversations. It’s important that we take responsibility for our impact on others**, without getting defensive as much as possible. **It’s also critical that we forgive those around us for their own mistakes and missteps** as we would like others to do for us. 

## Learn How to Be a Successful Ally: Four Key Practices

Notes from: https://trailhead.salesforce.com/content/learn/modules/workplace_equality_ally_strategies/we_equality_ally_become_champion

### The Four Equality Ally Practices

- **Ask** others about their experience and share yours
One of the seemingly smallest but also most powerful things we can do is ask others about their experience and how they are feeling. This can help you better understand the people you work with and their diverse experiences.  
 
Start with the people in your company. Ask someone new to coffee, raise your hand during a discussion, be inquisitive about issues that don’t necessarily impact you. That is the first step to being an ally. 

- **Listen** with empathy and seek to understand different perspectives
Empathy and mindful listening are critical components of allyship. As we have difficult discussions, careful listening helps us to have productive dialogue rather than circular debates. Practice your listening skills everywhere: While someone is speaking in a meeting, sharing their story, or presenting on an Equality topic.  
 
Use these tips from the National Center for Women in Technology (NCWIT) to help increase mindful listening:
>
- Make eye contact
- Express interest through your body language
- Ask thoughtful questions and ask for opinions
- Listen with empathy and compassion, rather than just listening for a chance to share your own story
 
There are other ways to “listen” too. Listening can include analyzing the content you are reading and researching in your spare time, particularly on social media [...] Diversifying your feed: following people in underrepresented groups so that you start to hear differing points of views and learn about a greater variety of issues. Then amplify their voices to others in your own circle. 

- **Show up** by being present, engaged, and commited
Once you have practiced asking questions and listening with empathy for a while, you can begin to understand what action is needed to help move a cause forward and feel more comfortable getting involved. Showing up is an extremely powerful way to show your support. This can mean showing up to an employee resource group meeting

- **Speak up** as an advocate and evangelize your allyship among others.
One of the bravest and most effective things an ally can do is speak up for someone. Speaking up can be uncomfortable and even scary. But **not speaking up can mean you agree with the injustice or harmful actions around you**.  
 
Here are some ways you can speak up for others:  
- Restate an idea that wasn’t recognized in a meeting, and give credit to the person who said it. 
- Confront a harmful comment by asking the person on the receiving end if they are okay and addressing the speaker by saying something like, “That wasn’t acceptable.” 
- Speak out about an injustice using your platforms (social media, blog, stage, and so forth). 
- Propose policy changes within your workplace or society that can create a better environment for all.

## Cultivate a Culture of Equality with Inclusive Leadership

> Notes from: https://trailhead.salesforce.com/content/learn/modules/inclusive-leadership-practices/cultivate-a-culture-of-equality-with-inclusive-leadership

### Defining Inclusive Leadership

A successful inclusive leader understands and elevates their team’s unique strengths, uses their platforms to advocate for their employees, and fosters an environment where everyone feels empowered to be their full, authentic selves.

This can manifest in the workplace as speaking up for someone who was interrupted in a meeting, creating space for difficult conversations, sharing your own Equality journey, or even changing the monthly team happy hours to a lunch so that it’s easier for caretakers to join.

### Five Steps to Becoming an Inclusive Leader

- Lead with Equality
- Have brave, authentic conversations
- Practice inclusive meetings
- Be fair in assignments and promotions
- Celebrate and bond with everyone in mind

## Understand the Business Impact of Inclusive Leadership

> Notes from https://trailhead.salesforce.com/content/learn/modules/inclusive-leadership-practices/understand-the-business-impact-of-inclusive-leadership

What does inclusion feel like? It feels like when you enter a room and you don’t have to check any part of yourself at the door. When your unique characteristics are celebrated. When you can raise your voice and be unequivocally heard. When you are surrounded by allies and advocates who use their platforms to lift you up. When opportunities are accessible and there are no boundaries to performing the best work of your career. 

## Practice the Five Principles of Inclusive Leadership

> Notes from https://trailhead.salesforce.com/content/learn/modules/inclusive-leadership-practices/practice-the-five-principles-of-inclusive-leadership


### 1. Lead with Equality

 - **Understand Your Team’s Unique Strengths**: Every person brings a unique advantage to the table. Some people are better at public speaking, while others are more skilled writers, for example. Or some may be driven by the big ideas, while others may be task-oriented. Understanding these skill sets and preferences can help you determine how to make a more comfortable and supportive environment for all.
 - **Support Flexibility**: Providing support for major life events, as well as varying family and home structures, enables everyone to have equal opportunities to succeed in the workplace. 
 - **Encourage Active Leadership and Growth**: You may find that your employees have deep passions connected to driving Equality. Allowing time for employees to participate in volunteer activities or Employee Resource Group events can help them develop useful business skills, network outside of their team, align them to a deeper sense of purpose, and create positive social change in your workplace and community. You can visibly show your support for these activities through recognition and by simply showing up as an Ally at these events.

### 2. Have Brave, Authentic Conversations

- **Listen with Empathy**: Listening is a key practice of allyship. Going a step further to listen with empathy is the difference between listening to respond and listening to understand. [...] If you take the time to listen with an open mind, it can dramatically shift the way you engage with employees as they become increasingly confident in using their voices and sharing their experiences.
- **Tell Your Story**: [...] To truly create a culture where employees feel empowered to share their journeys and bring their full selves to work, it is important to take the lead and share your own authentic Equality story.
- **Create a Culture of Transparency**: Create an environment where others feel empowered to speak up for one another and raise issues without fear. Establish platforms for employees to do so in a comfortable and safe way. Solicit regular feedback using anonymous survey tools, [...] and ensure that the process to report an issue is clear.
- **Encourage Healthy Discourse**: Be open to hearing differing views, and encourage others to express their opinions. Remember, discourse is healthy and helps drive innovation. [...] **Diverse groups outperform more homogeneous groups because diversity triggers more careful information processing that is absent in homogeneous groups.** Help drive innovation by ensuring that everybody is heard and all perspectives are valued.
- **Emphasize Accountability and Forgiveness**: [...] Encourage employees to take responsibility for their actions—understanding that though their intent may have been positive, it’s the impact that matters. At the same time, emphasizing forgiveness and establishing next steps to move forward from an uncomfortable situation is important. We all make mistakes and can approach our mistakes as opportunities to learn how we can all be better.

### 3. Practice Inclusive Meetings

 - **Make Sure Everyone Is Heard**: Do your best to ensure that everyone speaks in a meeting. If needed, prompt those who are quieter with questions such as, “What are your thoughts,” or “How would you approach this problem?” If someone is interrupted, make sure to address it in the moment. 
 - **Invite People to Have a Seat at the Table**: Are you meeting with the same subgroup every time? Bring in new voices from other groups to ensure diverse perspectives and ideas are being shared.
 - **Give Credit and Recognition**: It can be frustrating when someone proposes a great idea only to have someone else state the same idea and receive credit for it. Similarly, it’s disheartening to work hard on a project only for the credit to be given to someone else. [...] Visibly acknowledge the person who came up with the original idea or worked on the project and reinforcing their contribution.
 - **Rotate Note-taking**: This can seem small, but a common microaggression reported in the workplace is that women often end up taking notes for the meeting, and as a result, are not seen as leaders or vocal in that setting. To avoid this, rotate who takes notes to ensure it isn’t the same person every time.

### 4. Be Fair in Assignments and Promotions

 - **Spread High-Visibility Projects**: Ask yourself, “Are the same people on my team getting high-visibility projects over and over?” Give all members on your team the opportunity to step up.
 - **Consider Your Promotions**: Did everyone have an equal opportunity? Were all candidates evaluated fairly?
 - **Share the Promotion Process**: Is your promotion process transparent? Do people know what it takes to get promoted? Are professional development conversations taking place all year ’round?

### 5. Celebrate and Bond with Everyone in Mind

As a leader, be mindful of the way your team celebrates and bonds. Often, without realizing it, we leave people out because of the nature or time of the social activity. **For example, it can be harder for caregivers to attend a happy hour versus a lunch event because they have childcare obligations. Similarly, if the activity consistently involves playing a sport, such as soccer or softball, this can be isolating to those with physical disabilities.**

When planning a team activity, consider the time (for example, happy hour vs. lunch), the location and nature of the activity (for example, sports bar vs. team VTO event), and whether everybody is able to participate in and share their authentic selves during the chosen activity.

## Understand the Impact of Inclusive Marketing

> Notes from: https://trailhead.salesforce.com/content/learn/modules/inclusive-marketing-practices/understand-the-impact-of-inclusive-marketing

### Introduction

We recognize that we are not just serving our shareholders, but also all of our stakeholders, including our customers, employees, and community. A core part of how we all connect with the world is through our business practices, including marketing. 

### Understanding Inclusive Marketing

We define inclusive marketing as creating content that truly reflects the diverse communities that our companies serve. It means that we are elevating diverse voices and role models, decreasing cultural bias, and leading positive social change through thoughtful and respectful content. 

Our responsibility as marketers is to relay our brands’ messaging in a way that resonates with people from all backgrounds, regardless of race, ethnicity, gender identity, age, religion, ability, sexual orientation, or otherwise. 

Beyond diversity, truly inclusive marketing can elevate the stories and voices of people that have been typically marginalized or underrepresented, deepen connections with customers, and even influence positive social change. **Marketing can have a powerful impact on society—from our definitions of beauty, to what a scientist looks like, to what an athlete should be.** What if we started to see more inclusive images all around us?

### Why Inclusive Marketing Is Important

#### 1. Customers want to see themselves represented

**Consumers want to see themselves reflected in the brands they are interacting with.** In fact, 52% of consumers are likely to switch brands if a company’s messaging isn’t personalized and inclusive of them.

#### 2. Reach new audiences

With People of Color making up almost 24%, and women almost 51% of the population in the United States, **marketers miss out on a huge market opportunity by not including these communities in their advertising**. And with China and India making up 2.8 billion of the world’s population and rising, inclusive marketing has never been more relevant. 

#### 3. Attract the next generation workforce

**The workforce of the future will be increasingly diverse and expect more and more from the companies they work for.** Businesses that don’t reflect these changes will miss out on a tremendous pool of talent and opportunity to authentically connect with their diverse customer base. 

**Without a sharp focus on Equality and inclusion, companies can run the risk of appearing out of touch, disengaged, distant, or even offensive**. On the other hand, when companies do this well they build brand loyalty, deepen connections with their audiences, become destination workplaces for all populations, and lead their industry. 

## Introduce Inclusive Marketing to Your Company

> Notes from https://trailhead.salesforce.com/content/learn/modules/inclusive-marketing-practices/introduce-inclusive-marketing-to-your-company

### Intention vs. Impact

We like to begin all conversations about Equality by underscoring the phrase: intention vs. impact. This is the idea that **while the majority of the time we are all operating with positive intent, it’s the impact of our actions that truly matters.**

[...] When we consider the impact of our actions, rather than focusing on our own intent, that’s when we can move forward and create better, more inclusive content that resonates with all. At the same time, it’s important to remember that we are all learning together and we all have and will continue to make mistakes. But we can commit to being open, listening with empathy, continuing our education, and driving progress forward. 

### Defining the Six Principles of Inclusive Marketing

- Start with Tone
- Be Intentional with Language
- Ensure Representation
- Consider Context (historical and order)
- Avoid Appropriation
- Counter-Stereotype

## Learn the First 3 Principles: Tone, Language, and Representation

> Notes from: https://trailhead.salesforce.com/content/learn/modules/inclusive-marketing-practices/learn-the-first-three-practices

### 1. Start with Tone
Definition: Tone is the style, characteristic, or sentiment of a piece of content.

Often when people are offended or turned off by a piece of content but can’t quite put their finger on why they feel that way, tone is at the center. **The most common misstep is when something that should be treated seriously and with respect is treated too casually.**

Here are a few things to consider when discussing tone:

- **The subject**: Am I showing my subject in the best light? Am I honoring their legacy or work?
- **The topic**: Is the topic of the piece or conversation light-hearted or serious? What is the context?
- **The message**: What message am I trying to convey? Is it inspirational or controversial or educational or playful?
- **The impact**: What is my intended impact of the piece? Can it be perceived differently for any reason?

### 2. Be Intentional with Language
Definition: Language is defined as the words, phrases, symbols, or metaphors used to describe something.

[...] Sometimes, words may have different meanings depending on regional and cultural contexts. Language can be very nuanced, so it’s important to be mindful of how your messages are conveyed.

Consider this example: 

A skin-care company misstepped when a description of their lotion read, “For normal to dark skin.” Though potentially an honest mistake, the implication that dark skin isn’t normal caused a strong public reaction as many saw it as playing into cultural and historical stereotypes.

### 3. Ensure Representation
Definition: Representation is the visible presence of a variety of identities in a story, image, video, and more.

**People want to see themselves reflected in the media and marketing. When we see ourselves in the characters and images around us, it helps us all to feel seen, heard, empowered, and inspired.** And it’s not just about seeing people who look like us, people also want to see a more authentic representation of the world around them that celebrates all the ranges of diversity within our global society as well. 

## Learn the Next 3 Principles: Context, Appropriation, and Counter-Stereotyping

> Notes from: https://trailhead.salesforce.com/content/learn/modules/inclusive-marketing-practices/learn-the-next-three-practices

### 4. Consider Context
We look at context in two categories: historical/cultural and hierarchal.

Definition: Context is the past circumstances that inform the setting of an event (or in the case of marketing, a piece of content).

#### Historical and Cultural Context

**A marketing message doesn’t exist in isolation—its context needs to be considered.** A major beverage brand recently received backlash when their ad campaign made light of a protest and encounter with the police. The advertisement unfortunately overlooked the historical and cultural context of race relations in the US.

#### Order and Hierarchy

When it comes to creating inclusive marketing campaigns, another key element to consider is the order and hierarchy of the subjects in the ad. Let’s consider some examples:

**An ad from a major soap company showed a black woman using soap and a caucasian woman appearing in the next frame. While the ad itself was diverse, the order in which we see these two women invokes racist connotations that dark skin is seen as “dirty.”** The company has since issued an apology but we see how order, though nuanced, can still have an immense impact. 

**In another example, we see common stock images of workplaces in which male employees stand in an authoritative position next to a woman at her desk. The order and hierarchy here suggests that the woman is inferior to the man, is less knowledgeable, and needs him to explain something to her.** Again, this might not have been the intention of the photographer or the marketer, but it’s the ultimate impact of the message.

### 5. Avoid Appropriation

Definition: Appropriation (or cultural appropriation) is taking/using an aspect from a minority culture without knowing or honoring the meaning behind it. 

Here are a few guidelines to determine whether something is actually appropriating vs. respecting a culture. 

- **Minority vs. Dominant Culture**: Power dynamics play a huge role in defining what is or isn’t appropriation. Is the culture being referenced a minority or marginalized culture? Is it being referenced by a dominant culture? If the answer is yes, then be cautious about how the reference is used. Though it may seem like a small reference, there can be a painful historic reminder or cultural implications attached.
- **Profit vs. Disenfranchisement**: Connected to power dynamics is profit. Is the majority culture profiting from an element of a minority culture? Is the minority culture sharing in those profits or excluded from them? In many cases of appropriation, an aspect of a minority culture that was previously stigmatized is portrayed positively in a majority culture context and profited from, while the original creators don’t receive any recognition or benefit.
- **Authentic vs. Inauthentic**: Are the references authentic? Are they being presented by authentic members of the culture?
- **Honoring vs. Exploiting or Making Fun**: Are the references paying homage to a culture? It is respectful? Is it helping to educate others about the rich history? Many times when we see instances of appropriation, it stems from attempting to be humorous. However it becomes disrespectful when sacred aspects of a culture are used for a punchline.
- **Informed vs. Misinformed**: Before using any element of a culture we must ask ourselves: do we understand the culture well enough to portray it in an ad? Always seek counsel from authentic voices in the community and do due diligence to learn the history and significance of the aspects you are referencing.

### 6. Counter-Stereotype
Definition: To counter-stereotype means going against a standardized image that represents an oversimplified opinion, prejudiced attitude, or uncritical judgment.

This is an area where we as marketers have the power to influence society around us. Marketers can lead their own Equality movement by helping diverse people see their own potential reflected in the world around them. **Imagine if we showed more women as CxOS, people of color as the head engineers, people with disabilities as the sports icons, for example.**

## Develop an Inclusive Review Process

> Notes from: https://trailhead.salesforce.com/content/learn/modules/inclusive-marketing-practices/develop-an-inclusive-review-process

### Put On Your Equality Glasses

The first step in the final review process is to examine your content carefully through the lens of the inclusive marketing principles.

### Have a Diverse Review Panel

To truly create diverse marketing that reflects our communities, it’s important that our workplaces reflect society as well. 

Before sharing content, ensure a diverse panel from different identities have both created and reviewed the material. Be aware of the intersectionality and complexity of the human experience. 

However, it’s also important that we don’t burden minority groups with being the ones to speak up. Train and engage everyone in  the inclusive marketing principles and encourage all to wear their “Equality glasses.” 

### When in Doubt, Seek Input

It’s important that we listen to the voice within that tells us when something doesn’t feel right. In many of the examples we reviewed, these mistakes could have probably been avoided both if someone spoke up or if they were empowered to speak up.

### Create Space for Inclusive and Anonymous Feedback

As we mentioned earlier in this module, these conversations can be difficult to have. Especially if you are a team leader, it’s important to create a space where people feel safe to speak up and dissent. If you are a more senior person in the room, before you give your input, make sure you hear from everyone in the meeting, and make it clear that it is okay to disagree or point out something wrong. 

For underrepresented minorities especially it can be intimidating and scary to speak up if you are one of few or the only one in the room. This is why it’s important to have a vehicle for anonymous feedback—it can be as simple as an anonymized survey people fill out after the meeting with their thoughts on the content. 

### Prioritize Accessibility

Creating marketing that is truly inclusive also means meeting accessibility standards and needs. **With 15% of the world’s population having a disability, incorporating accessibility means reaching a broader audience. When it’s broken down, the word accessibility is the “ability” to “access.”**