cd ~/workspaces/planning &&
git add --all &&
git commit -m "Update notes @ $(date "+%a %d/%m/%y %H:%M")" &&
git pull origin main &&
set -u &&
git push origin main &&
echo 'Done comitting Notes' 