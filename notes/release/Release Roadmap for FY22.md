# Release Roadmap for FY22

### Release Group Description

Our design mission is to bring simple, clean ways to make Gitlab the tool of choice for deploying where, when, and how users want.

We work to ensure the best user experience for our users of all knowledge levels, allowing them to successfully apply the continuous methods (Continuous Integration, Delivery, and Deployment) to their software with no third-party application or integration needed.

Our strategy is all about making sure that even complex delivery flows become an effortless part of everyone’s primary way of working.

The definitions of done consists of a list applied by the Product Designer and Product Manager to issues or epics, in order to align and track the entry and exit criteria for design work.e


[See roadmap](https://gitlab.com/gitlab-com/Product/-/issues/1906)
[See video-walkthrough](https://www.youtube.com/watch?v=EgQ9CgC9qbE)

### Continuous Delivery
- [[DORA4 Metrics]]
	- Make DORA metrics visible and easy to track on the project / org level
	- Enable finer controls and visualization in the future
-  [[Increase Discoverability for CD features]]
	-  Improve in-product guidance for CD features
	-  Small tweaks to increase adoption and reduce friction
-  [[Better Enable Multi-Cloud Deployments]]
	-  Industry is moving towards this direction, and we should better-suport this use case
-  [[Group-level resource group]]
	-  Limit number of deployments that can be made against a specific target -- now able to be setup on the group level
	-  See more: https://gitlab.com/gitlab-org/gitlab/-/issues/122010
-  [[Auto/manual Environment freeze]]
	-  Have environments frozen based on an ongoing alert or manually
	-  Problem to solve: As a developer deploying new code, I would want to cancel deployment if the new code exceeds a specific error threshold rate, so that I do not expose faulty code to a larger audience.

### Advanced Deployments
 - [[Advanced Deployments for AWS]]
	 - Blue/Green and Canary deployments for non k8s users