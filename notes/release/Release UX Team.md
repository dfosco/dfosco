---
layout: handbook-page-toc
title: "Release UX Team"
description: "The Release UX team's goal is to design simple, clean ways to make GitLab the tool of choice for deploying where, when, and how users want to."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

The [Release stage](/stages-devops-lifecycle/release/) includes all features that help you guarantee software delivery by automating the release and delivery of applications, shortening the delivery lifecycle, streamlining manual processes, and accelerating team velocity.

The Release UX team's goal is to enable these complex flows by providing the best experience in software delivery. Our design mission is bring to the forefront simple, clean ways to make GitLab the tool of choice for deploying where, when, and how users want to.

Our biggest partners are the stages under the Ops section ([Verify](/direction/ops/#verify), [Package](/direction/ops/#package), [Configure](/direction/configure/) and [Monitor](/direction/monitor/)), Dev section ([Manage:Optimize](/direction/dev/#manage-1)), and Infrastructure ([Delivery](/handbook/engineering/infrastructure/team/delivery/)).

### Release UX Team

[Daniel Fosco](https://gitlab.com/dfosco) - Senior Product Designer
[Rayana Verissimo](https://gitlab.com/rayana) - Product Design Manager
[Achilleas Pipinellis](https://gitlab.com/axil) - Senior Technical Writer

### Stable counterparts

The following members of other functional teams are our stable counterparts:

- [Nicole Williams](https://gitlab.com/nicolewilliams) - Engineering Manager
- [Kevin Chu](https://gitlab.com/kbychu) - Group Product Manager


### Shared UX

The user experience around releasing code from GitLab repositories spans many areas and pages of our product. In order to deliver a seamless user experience, our team covers the following areas:

- Environments
- Releases
- Feature Flags
- Review Apps
- gitlab-ci.yml
- CI Pipelines

Other areas and pages may also include: 

- Project settings
- Merge Requests
- Infrastructure & Kubernetes
- Runners
- Audit log

### Feature Map

We also keep a visual map of features that includes key screens from the list above. It's a living document that serves as a reference for how our experience looks.

<div class="figma-embed" aria-label="Embed description" role="img">
<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2Fm86hcciGMQJ4RzvVq6Wlnu%2FRelease-Stage-Feature-Mapping%3Fnode-id%3D0%253A1" allowfullscreen></iframe>
</div>

### Our Users

We have different user types we consider in our experience design effort. Even when a user has the same title, their responsibilities may vary by organization size, department, org structure, and role. Here are some of the people we are serving:

- [Release Manager](/handbook/marketing/strategic-marketing/roles-personas/#rachel-release-manager)
- [Software Developer](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
- [Development Tech Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
- [DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
- [Product Manager](/handbook/marketing/strategic-marketing/roles-personas/#parker-product-manager)
- QA

### Vision

The best way to understand the strategy and vision behind the Release stage is to read the [Deployment Direction](/direction/deployment) handbook page.

The current vision for Release ...

<!--

The product vision for Release has become more focused on providing advanced administration capabilities for release coordination and deployment tracking in GitLab. This is to build on the data asset we have at GitLab that starts from users purchasing GitLab to build product fast in a continuously integrated way. We will expand this journey by helping them coordinate and deploy at scale.

Today, mono-repository projects deploying with Kubernetes are most able to take advantage of our offering. We are targeting customers needing to coordinate across many teams and groups to successfully deploy. Regulated industries are top benefactors of our offering.

-->

### UX strategy

We will commit to stay aligned on shared UX with the engineering groups as much as possible, being the conversation drivers with product managers and other counterparts.

The Release UX team is working together to uncover customers' core needs, what our users’ workflows look like, and defining how we can make tasks easier. Our strategy involves the following actions:

| Strategy | Cadence |
| -------- | ------- |
| Jobs to be done framework | Quarterly reviewed |
| [UX Scorecards and recommendations](/handbook/engineering/ux/ux-scorecards/) | Ad hoc |
| [Opportunity canvas](/handbook/product/product-processes/#opportunity-canvas) | Ad hoc |
| Stakeholder interviews | Ad hoc |
| User and customer interviews | Ad hoc |

Visit [CI/CD UX](/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/) page to read about the department strategy.

### Jobs To Be Done

See all [Release stage](/handbook/engineering/development/ops/release/jtbd/) JTBDs.

### Team meetings

- **Release Group Weekly**: Once a week, meeting with engineering team to align ongoing development efforts
- **Release UX/PM Sync**: Once a week, meeting between designers and product manager to align current and future design work
- **CI/CD UX Meeting**: Every two weeks. Meeting to discuss our stages UX shared efforts, review designs, and iterate on our strategy.

### Follow our work

The [Release UX YouTube channel](https://www.youtube.com/playlist?list=PL05JrBw4t0KoyqCjN4f79w0dYZusHLx15) includes UX Scorecard walkthroughs, UX reviews, group feedback sessions, team meetings, and more.