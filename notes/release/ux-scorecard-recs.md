Don't know how to create a release


Not sure what I'm going to do, so I google it. “How to create a release with GitLab”, step by step on google from SO

From StackOverflow, Really easy compared to the GitLab docs, I prefer to follow this. If it's highlighted by Google, it should work.

Go to repository, left navigation and go to tags

Try to create a new tag, check what I need to put inside the Release. 

I'm not creating the tag -- *not sure what the relationship is between tag and release* 

*I feel like I should create the tag first, and then the release*

I start doing this, tag name is obvious, maybe there's somewhere I can put the link in?

*I was confused... the instruction tells me to create a tag, thus you create a release?*

*Can I find the JSON evidence on the tag artifacts download? I don't think so*

*Maybe this time Google is wrong... goes to GitLab Docs and figures out to go on Deployments > Release to create a new release*

*Obviously at this stage I'm a little bit disappointed that I went to two instructions and found the other one that wasn't what I wanted to do... what I did before might be a waste of time*

*Saw release named v0.1.5 -- is this the tag I created?*

*Link information is really good*

“Something went wrong when creating the Release” -- very vague error message

Tried to remove some of the information, delete the link, add a new link, to see if the message disappeared. I cannot create a Release, message did not tell me what happened.

What exactly is the relationship between tag and release? Should I delete the tag because it's messing it up?

*Deleted the tag* -- Now I cannot create a release, because I need to select a tag!

I already deleted my tag at this stage, so I went again to create a tag. So I repeat what I did. Stuck in a cycle now!

Try it again, get another error message. This didn't go well for my emotional state.

OK, let me read the docs carefully because I'm really not the kind of person that will read docs in detail. Google is short and step by step.

I quickly scan it and see a tag cannot be associated with a release already, to create another release

Dropdown to search for tag **and create a new one... doesn't work**

Go back, delete a tag again and re-create it.

And I realize, **when you create a tag... a release is created already 🤯🤯🤯**

Now I cannot know that creating a release is equal to creating a tag... and I'm still not 100% sure because I read the official GitLab doc and it still tells me a tag can be associated with a Release or not.

*Here, to delete the release, but not the tag. Or remove the tag from the release... I didn't find a way.*

What I found was: if I delete the tag, the release is gone. **I cannot just delete the release**

I think this is still confusing for me, but considering the task to create the release, I think I did create the release.

The evidence JSON file is clear what I need, what I need to download.

Looking from above: following the Google instructions from the beginning was ok, but GitLab 

*Not explaining the relationship between releases and tags is very frustrating*

The UI behavior still confused me, it didn't map 100% what the doc say, the tag can be associated with a release. The deleting of the tag removes the release. It gave me the impression those two are a 1:1 mapping

If there are two ways to create a release, either creating a tag or creating a release, communicate that.

Clearly communicate the relationship between release and tag.

It feels like the tag is more important than the release in a way.

This error message is _really_ frustrating. It just told me "it went wrong".

**This process needs to be improved, creating a tag within the dropdown is really hidden.**

**Creating a tag and creating a release means jumping between two areas of the navigation, and that can be improved**

**The task is solvable, but time-consuming, so made me as a new user frustrated. I do see clearly how it could be improved.**

### Issues

1. Make relationship between tags and releases clearer in the documentation
 - The documentation does not make it clear that tags can generate releases and vice-versa
 - [Create a release](https://docs.gitlab.com/ee/user/project/releases/#create-a-release)
 - [Add release notes to Git tags](https://docs.gitlab.com/ee/user/project/releases/#add-release-notes-to-git-tags)
 
2. Make relationship between tags and releases clearer in the UI
 - Make "create tag > create release" behavior more obvious
 - Make "delete tag > delete release" behavior more obvious

3. Make error messages on Release creation clearer

3. Move "create tag" action on Release page out of the dropdown for more clarity


### Support users by showing their steps towards a full release/deployment workflow