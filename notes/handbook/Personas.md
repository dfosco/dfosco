### Organizational Models

Organizational models describe how organizations have structured their software development process and teams. They help us understand common structures and the resulting types of interactions that the personas in those structures experience.

### Personas

#### [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#cameron-compliance-manager)

I’m in charge of ensuring that our organization’s use of third-party software adheres to internal company policy. [...] I interface regularly with our internal compliance, audit, and/or security teams to deliver the information they need for our organization’s compliance program.

[[Jobs to be Done]]

-   I need to be able to provide our internal compliance teams with evidence artifacts that help my company maintain a positive compliance posture.
-   I need to find tools that enable my organization to manage our compliance program and mitigate risk within the application and its use.
-   I need to create effortless processes for compliance so that my team will remain productive and efficient while meeting obligations for our primary job responsibilities