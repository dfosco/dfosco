1.  **Always open a MR** for things you are suggesting and/or proposing. Whether something is not working right or we are iterating on new internal process, it is worth opening a merge request with the MVC instead of opening an issue encouraging open feedback on the problem without proposing any specific change directly.
2.  Never ask someone to create an issue when they can **default to the merge request**.
3.  Starting with a Merge Request is part of Handbook First and helps ensure the handbook is up-to-date when a decision is made. It is also how we make it possible for Everyone to Contribute. This is true, not just for updating the handbook but for updating all things.
4.  Merge Requests, by default, are non-confidential. However, for things that are not public by default please open a confidential issue with suggestions to specific changes that you are proposing. The ability to create Confidential Merge Requests is also available. When possible, consider not including sensitive information so the wider community can contribute.
5.  **Not every solution will solve the problem at hand.** Keep discussions focused by **defining the problem first and explaining your rationale** behind the MVC proposed in the MR.
6.  Be proactive and consistent with communication on discussions that have external stakeholders such as customers. It’s important to keep communication flowing to keep everyone up to date. **MRs can appear stale if there aren’t recent discussions and no clear definition on when another update will be provided, based on feedback.** This leaves those subscribed in the dark, causing unnecessary surprise if something ends up delayed and suddenly jumps to the next milestone. **It is important that MRs are closed in a timely manner through approving or rejecting the open requests.**
7.  Have a bias for action and don’t aim for consensus. Every MR is a proposal, if an MRs author isn’t responsive, take ownership of it and complete it. Some improvement is better than none.

...

10.  If submitting a change for a feature, **update the description with the final conclusions (Why an MR was rejected or why it was approved)**. This makes it much easier to see the current state of an issue for everyone involved in the implementation and prevents confusion and discussion later on.
    
11.  **Submit the smallest item of work that makes sense. When proposing a change, submit the smallest reasonable commit**, put suggestions for other enhancements in separate issues/MRs and link them. **An MR can start off as only a problem description and TODO comments**. If you're new to GitLab and are writing documentation or instructions, submit your first merge request for at most 20 lines.
    
12.  **Do not leave MRs open for a long time.** MRs should be actionable – stakeholders should have a clear understanding of what changed and what they are ultimately approving or rejecting.
    
13.  **When submitting a MVC, ask for feedback from your peers.** For example, if you're a designer and you propose a design, ping a fellow designer to review your work. If they suggest changes, you get the opportunity to improve your design and propose an alternative MR. This promotes collaboration and advances everyone's skills.
    
14.  **Respond to comments within a threaded discussion.** If there isn't a discussion thread yet, you can use the Reply to comment button from the comments to create one. **This will prevent comments from containing many interweaves discussions with responses that are hard to follow.**
    
15.  **If your comment or answer contains separate topics, write separate comments for each**, so others can address topics independently using the Reply to comment button.
    
16.  **If you have received any feedback or questions on your MR, try to acknowledge comments as that's how we ensure we create an environment of belonging for all team members.** Merging your MR as-is without giving an answer or any response makes the commenters feel their opinions are unheard.
    

**If you are the DRI who does not have to make a fast decision, you can choose not to change your MR, but you should acknowledge the comments or feedback**, consider if they warrant a change to your MR, and say why, not just what.

**If there are many comments, you can choose to summarize key feedback areas** and share your response at a high level.

We appreciate that **if you force a DRI to explain too much, you'll create incentives to ship projects under the radar.**

**The fear of falling into a perpetual loop of explaining can derail a DRI, and cause people to defer rather than working with a bias for action.** This is something we want to avoid.

**When fast decisions are needed, we'll have to accept that people listened to us but don't owe us an explanation to have fast decisions based on everyone's input.** The goals are to be transparent and collaborative–not to lose efficiency. **Not everyone will agree, but we expect all people to disagree, commit, and disagree.**

19.  **Even when something is not done, share it internally so people can comment early and prevent rework.**
    
20.  Create a Work In Progress (WIP) merge request to prevent an accidental early merge. **Only use WIP when merging it would make things worse, which should rarely be the case when contributing to the handbook**. Most merge requests that are in progress don't make things worse. In this case, don't use WIP; **if someone merges it earlier than you expected just create a new merge request for additional items**.
    

**Never ask someone to do a final review or merge something that still has WIP status. At that point you should be convinced it is good enough to go out.**

22.  **If a project requires multiple approvals to accept your MR, feel free to assign multiple reviewers concurrently.** This way the earliest available reviewer can start right away rather than being blocked by the preceding reviewer.
    
23.  **If the MR involved gets a lot of comments**, you can turn it into a Manager Mention MR.
    
24.  Consider recording a concise video or audio file outlining the merge request and uploading it to the GitLab Unfiltered channel on YouTube. This will make content more accessible, prevent future confusion, allow for multitasking (e.g. cooking dinner and listening to the video), and increase participation for folks who digest audio information better than visual.