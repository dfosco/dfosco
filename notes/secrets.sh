cp -R ~/workspaces/planning/notes/confidential/* ~/workspaces/notes-confidential/ && 
cd ~/workspaces/notes-confidential/ && 
git add --all && 
git commit -m "Update secret notes @ $(date "+%a %d/%m/%y %H:%M")" && 
git pull origin main &&
set -u &&
git push origin main
echo 'Done comitting Secret Notes'