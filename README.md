# Hi, Daniel here 👋

My name is Daniel Fosco, I'm a Senior Product Designer working on the [Release Group](https://about.gitlab.com/handbook/product/categories/#release-group) at GitLab.

This README is meant to help people that work directly with me at GitLab to know a bit more about me, how I work, and how I do things.

🦊 [GitLab User](https://gitlab.com/dfosco)  
👯‍♂️ [Team Page](https://about.gitlab.com/company/team/#dfosco)  

This README is still in v1, so let me know if you have suggestions for what else to put here 😉

### What I do

Being a product designer for me means understanding the context and problems my users face, and do my very best to discover the solutions that will make their lives easier. Working with developer and professional tools has been a focus of mine for quite some time. 

Here's a list of things I enjoy working with as a designer, in no particular order:

- Coding in HTML and CSS
- Interviewing users
- Sweating the details in iteractions
- Talking to other designers
- Defining long-term design strategy

If that seems very broad... well, I have very broad interests when it comes to design 😅

### My Availability

 - I generally work from ~10:00 to ~18:30 CET. You will see my calendar blocked with a Busy event whenever I take personal time during the day. If you see a good time on my calendar inside my working hours, please schedule something (no need to ask first). You can do so either via Google Calendar (preferred) or using my Calendly page.

 - I usually do not accept meetings late-nights or early mornings. Friday is my (mostly) meeting-free day. If you need to reschedule something, go ahead! All my calendar events are set with modify privileges and you should feel free to go ahead and move it to some other time slot that is within business hours and not conflicting with other meetings.

 - Slack DM or mention is probably the best way to reach me during the day. I also read my emails multiple times a day. You should not feel obligated to read or respond to any messages you receive from me outside of your normal hours.

 - Please @ me directly in comments in GitLab so I see your message, and assign me as reviewer in MRs I should review. 
 
 - I use my email inbox to manage my to-do list, and I have a [rolling personal backlog](https://gitlab.com/dfosco/dfosco/-/issues) that's public on GitLab.

### Collaborating with Me

 - I like being involved in the entire product and development process for my team. That doesn't mean I will work on _literally everything_, but understanding context and being able to provide feedback for my peers is essential for my design work.

 - I thrive when I'm able to break down work into pieces small enough that I can confidently execute on — research, design, or code.

 - I'm very open to feedback, even if we're not working super closely. If there's one interaction or input from me you think were less than ideal, I'd love to hear it. Feel free to DM me on Slack or inviting me for a quick coffee chat.

### Who I am

I'm born and raised in Rio de Janeiro, Brazil ☀️🌴

Originally went to university to study Book Publishing, and after graduation I somehow ended up working at a tech company, and it went downhill from there. I've been working with Web / UX / Product Design in different capacities for the past 9 years, and it's been quite a ride! I have a [portfolio thingy](https://danielfos.co) you can see at if you like those.

In 2017 I moved to the Netherlands with my partner and we've been enjoying the rainy/sunny/rainy weather in Amsterdam ever since!

Things I like to do in my free time: 

- [Skateboarding](https://mobile.twitter.com/dfosco/status/1359438781573652480)  
- Cooking for my friends  
- [Playing video-games](https://mobile.twitter.com/dfosco/status/1355989586317553675)  
